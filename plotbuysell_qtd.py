## Dashboard application

## Author: Andre Vieira da Silva 2020-10-16

import plotly.graph_objects as go
import dash
import dash_html_components as html
import dash_core_components as dcc
import pandas as pd
import plotly.offline as pyo
from dash.dependencies import State, Input, Output

dcc.Dropdown()
options_list=[
            {'label': 'Percentage', 'value': 'per_buyers'},
            {'label': 'Quantity', 'value': 'per_sellers'},
            ]


# Dash application
app = dash.Dash()

### define the HTML component/layout
### for more options CSS, please see:
### https://www.w3schools.com/css/default.asp
app.layout = html.Div(children=[html.Div("Dashboard app",style= {   "color": "white",
                                                      "text-align": "center","background-color": "black",
                                                      "border-style": "solid","display":"inline-block","width":"100%"    
                                                    }),
                       html.Div(dcc.Dropdown(id = "drop_down_1" ,options= options_list , value= 'Percentage'),
                                             style= {
                                                      "color": "black",
                                                      "text-align": "center","background-color": "black",
                                                      "border-style": "solid","display":"inline-block","width":"20%"   
                                                    }),
                       html.Div(children=[html.P(id="map-title",
                                                 children = "Cryptocurrency",
                                                ), html.Div(dcc.Graph(id ="plot_area"))
                                                       ],style = {
                                                      "color": "white",
                                                      "text-align": "center","background-color": "black",
                                                      "border-style": "solid","display":"inline-block","width":"75%",
                                                    })],style={"width":"100%",'paffing':10})

## Creating callback buttons
@app.callback(Output("plot_area", 'figure'),
              
              [Input("drop_down_1", "value")])

def updateplot(input_cat):

    # read csv file -> dataframe
    df = pd.read_csv('data/infoBuyersSellers.csv')

    # get dataframe and make the plot
    def fig_generator(sample_data):
        plot_data=[]
        plot_data.append(go.Scatter(x = ((sample_data.index)*5)/3600, y = sample_data['per_buyers'], name="Buyers"))
        plot_data.append(go.Scatter(x = ((sample_data.index)*5)/3600, y = sample_data['per_sellers'], name="Sellers"))
        
        # set layout plot
        plot_layout = go.Layout(title="Statistics: Buyers versus Sellers") 
        fig =go.Figure(data = plot_data, layout = plot_layout)
        fig.update_layout(
            title="% Buyers/Sellers vs time",
            xaxis_title="Time (hours)",
            yaxis_title="Percentage",
            legend_title="",
            font=dict(
                family="Courier New, monospace",
                size=16,
                color="black"
            )
        )
        #fig.show()
        return(fig.data,fig.layout)
    
    sample_data = df
    
    trace,layout = fig_generator(sample_data)
    
    return {
        'data': trace,
        'layout':layout
    }
    
# run app server
if __name__=='__main__':
    app.run_server()
