# importing the libraries
import numpy
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import numpy as np
import pandas as pd
import time
from itertools import count
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import os.path
import sys

plt.style.use('fivethirtyeight')

index = count()

if (len(sys.argv) < 1):
    print("Number of arguments is wrong, sorry, check this please.")
    exit()

CRYPTOCURRENCY=sys.argv[1]
print("Cryptocurrency:.............. ", CRYPTOCURRENCY)

CRYPTOCURRENCY=sys.argv[1]

# animate to update frequently
def animate(i):
    # Open a file: file
    file = open("data/infodata.txt", "r")

    # array to store data to plot
    vec_infodata= numpy.zeros(shape=(3,2))

    # read line by line from infodata.txt
    iline=0
    for line in file:
        vec_infodata[iline][0] = float(line[0:5])
        vec_infodata[iline][1] = float(line[6:11])
        iline =iline+1

    # variables
    per_buyers = vec_infodata[0][0]
    per_sellers = vec_infodata[0][1]

    per_qtd_buyers = vec_infodata[1][0]
    per_qtd_sellers = vec_infodata[1][1]

    per_biggest_buy = vec_infodata[2][0]
    per_biggest_sell = vec_infodata[2][1]

    # plot:
    labels = ['','','']
    buy_means = [per_buyers, per_qtd_buyers, per_biggest_buy]
    sell_means = [per_sellers, per_qtd_sellers, per_biggest_sell]

    colors_buy = ['g','g','g']
    colors_sell = ['r','r','r']

    x = np.arange(len(labels))  # the label locations
    width = 0.1  # the width of the bars

    plt.cla()

    fig, ax = plt.subplots()
    rects1 = ax.bar(x - width, buy_means, width, label='Buyers', color=colors_buy)
    rects2 = ax.bar(x + width, sell_means, width, label='Sellers', color=colors_sell)

    # Add some text for labels, title and custom x-axis tick labels, etc.
    ax.set_ylabel('Percentage (%)')
    ax.set_title('Bitcointrade: ETH')
    ax.set_xticks(x)
    ax.set_xticklabels(labels)
    ax.legend()

    def autolabel(rects):
        """Attach a text label above each bar in *rects*, displaying its height."""
        for rect in rects:
            height = rect.get_height()
            ax.annotate('{}'.format(height),
                        xy=(rect.get_x() + rect.get_width() / 2, height),
                        xytext=(0, 3),  # 3 points vertical offset
                        textcoords="offset points",
                        ha='center', va='bottom')

    autolabel(rects1)
    autolabel(rects2)

    fig.tight_layout()

    # indicator: force.
    if(60>= per_buyers >= 50):
        plt.text(1.8, 40., "moderate")
    if(70 >= per_buyers >= 60):
        plt.text(1.8, 40., "strong")
    if(per_buyers >= 70):
        plt.text(1.8, 40., "very strong")
    if(40 <= per_buyers <= 50):
        plt.text(1.8, 40., "attention")
    if(30 <= per_buyers <= 40):
        plt.text(1.8, 40., "weak")
    if(20 <= per_buyers <= 30):
        plt.text(1.8, 40., "very weak")

    plt.savefig('scraping.pdf')
    plt.close()
    #plt.show()

    Tasks = [per_buyers,per_sellers]

    my_labels = 'Buyers','Sellers'
    my_colors = ['green','red']
    plt.pie(Tasks,labels=my_labels,autopct='%1.2f%%', colors=my_colors)
    plt.title(f'Percentage: Buyers versus Sellers - {CRYPTOCURRENCY}')
    #plt.axis('equal')
    plt.savefig('perbuyerssellers.pdf')
    plt.tight_layout()

ani = FuncAnimation(plt.gcf(), animate, interval=1000)

plt.tight_layout()
plt.show()

print("save plot, please and all done!")
