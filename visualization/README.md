# Data visualization in python

Documentation for data visualization using matplotlib, pandas. Below we have a nice tutoriais with the source code to plot.

[Bokeh](https://docs.bokeh.org/en/latest/index.html) is an interactive visualization library for modern web browsers.

### Useful Resource

- [Python Data Visualization with Matplotlib — Part 1](https://towardsdatascience.com/visualizations-with-matplotlib-part-1-c9651008b6b8)
- [5 Powerful Tricks to Visualize Your Data with Matplotlib](https://towardsdatascience.com/5-powerful-tricks-to-visualize-your-data-with-matplotlib-16bc33747e05)
- [Data Viz with Python: Apps & Dashboards](https://medium.com/swlh/data-viz-with-python-apps-dashboards-6693f46f5e6a)