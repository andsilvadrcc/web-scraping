# A simple Example
# author: Andre V Silva

# data visualization

# library
import numpy as np
import matplotlib.pyplot as plt

N=50

# variables
x = np.linspace(0., 10., N)
y = np.sin(x)**2 + np.cos(x)

# plot
# to change the figure size
plt.figure(figsize=(7, 4.5))
plt.scatter(x, y)

# To use LaTeX font as your default 
# font in Matplotlib
plt.rcParams['text.usetex'] = True

# set label plot
# marker = 'x'
# matplotlib.markers
# https://matplotlib.org/api/markers_api.html
#plt.scatter(x, y,s=15, color= 'r', label = r'$y  = sin^2(x) + cos(x)$')
plt.scatter(x, y, color= 'r', label = r'$y  = sin^2(x) + cos(x)$')

# axis scale is at the same scale
plt.axis('equal')

# label title axis
plt.xlabel(r'$x$ (rad)')
plt.ylabel(r'$y$')

# show legend
plt.legend()

# save plot pdf
plt.savefig('scatter2.pdf', dpi = 300, bbox_inches = 'tight', facecolor='w')

# draw plot
plt.show()
