from bs4 import BeautifulSoup
import requests
import pandas as pd 
import json
import time

message="Scraping cryptocurrency price"
print(message)

print("Searching...")

cmc = requests.get('http://coinmarketcap.com/')
soup = BeautifulSoup(cmc.content, 'html.parser')

#print(soup.title)

#<title class="next-head">Cryptocurrency Market Capitalizations | CoinMarketCap</title>

#print(soup.prettify())

data = soup.find('script', id="__NEXT_DATA__", type="application/json")
                 
print("data, Loaded!")

coins = {}

# using data.contents[0] to remove script tags
coin_data = json.loads(data.contents[0])
listings = coin_data['props']['initialState']['cryptocurrency']['listingLatest']['data']

for i in listings:
    coins[str(i['id'])] = i['slug']
    
# Putting everything together with pandas
market_cap = []
volume = []
timestamp = []
name = []
symbol = []
slug = []

for i in coins:
    url = 'https://coinmarketcap.com/currencies/' + coins[i] + '/historical-data/?start=20200101&end=20200630'
    #print(url)
    #print(f"coin: {coins[i]}")
    if(coins[i] =='tether'):
        print("stop the requests, to avoid the error response 429")
        break
    print("requests...")
    page = requests.get(url)
    soup = BeautifulSoup(page.content, 'html.parser')
    #print(type(soup))
    data = soup.find('script', id="__NEXT_DATA__", type="application/json")
    #print(type(data))
    historical_data = json.loads(data.contents[0])
    quotes = historical_data['props']['initialState']['cryptocurrency']['ohlcvHistorical'][i]['quotes']
    info = historical_data['props']['initialState']['cryptocurrency']['ohlcvHistorical'][i]
    

    for j in quotes:
        market_cap.append(j['quote']['USD']['market_cap'])
        volume.append(j['quote']['USD']['volume'])
        timestamp.append(j['quote']['USD']['timestamp'])
        name.append(info['name'])
        symbol.append(info['symbol'])
        slug.append(coins[i])
    
# dataframe

df = pd.DataFrame(columns = ['market_cap', 'volume', 'timestamp', 'name', 'symbol', 'slug'])

df['market_cap'] = market_cap
df['volume'] = volume
df['timestamp'] = timestamp
df['name'] = name
df['symbol'] = symbol
df['slug'] = slug

print(df.head())

# save pandas in csv file
df.to_csv('data/criptoes.csv', index=False)



