<!-- 
Simple example to do "web scraping".
Author: Andre Vieira da Silva
date 2020-09-23
-->

# Web-scraping

Web scraping page from internet, or web data extraction is data scraping used for extracting data from webpage.

This idea is an important tool for:

 - Data science
 - Big data
 - Data Analytics

## Web Scraping with Python

In this example, I create a simple script in python by using the library 
[BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/) to extract data 
from web page by parsing the data and make the plot by using [matplotlib](https://matplotlib.org/) and [pandas](https://pandas.pydata.org/) for visualization with python. I also use API (Application Programming Interface) based on the documentation from the [website](https://apidocs.bitcointrade.com.br/).  

### To install dependencies

If you do not have BeautifulSoup4, requests, matplotlib and numby installed, the command line below allow you to 
install the right dependencies. So, type the command line in your terminal to install by using pip:

```
pip3 install beautifulsoup4
pip3 install requests
pip3 install matplotlib
pip3 install numby
pip3 install pandas
```

## Basic instructions to use

```
To run this example:
$ git clone https://gitlab.com/andsilvadrcc/web-scraping.git
$ cd web-scraping
$ ls
infodata.txt  plotdata.py  README.md  scraping.pdf  scraping.py

# Run:
$ python scraping.py                                                                                      733ms 
Starting to Web scraping!
URL has successfully opened!
Number of buyers =  0.484
Number of sellers =  0.516
qtd. total buy  =  0.59856
qtd. total sell =  0.40144
total amount buy  =  85.66716
total amount sell =  57.45513
the biggest amount buy  =  6.22035
the biggest amount sell =  7.62289
percentage of the biggest amount buy of the total  =  0.07261
percentage of the biggest amount sell of the total =  0.13268
data save! all done, :) 

# output --> infodata from webpage.
# Now you can plot the data using:
$ python plotdata.py
```

## Result:

![](scraping.png)

**Figure 1**: Data visualization in real time.

![](perbuyerssellers.png)

**Figure 2**: Percentage of buyers versus sellers.

## Run app
![](ezgif.com-video-to-gif.gif)

# Dashboard application

Dash is a productive Python framework for building web applications.

Written on top of :
  - Flask 
  - Plotly.js
  - React.js

Dash is ideal for building data visualization apps with highly custom user interfaces in pure Python. It's particularly suited for anyone who works with data in Python.

Through a couple of simple patterns, Dash abstracts away all of the technologies and protocols that are required to build an interactive web-based application. Dash is simple enough that you can bind a user interface around your Python code in an afternoon.

**Dash apps are rendered in the web browser!** This is awesome, great!

Below a example of how Dash works. I am developing my own Dashboard to show the data from web scraping.
![](dashapp.png)

## Script python in Jupyter Notebook:

  - [scraping_JupNot.ipynb](scraping_JupNot.ipynb): Version interactive to see how the code works, have fun!

# Web scraping history BTC/USD
  - [getdata.py](getdata.py): Simple example data extract by parsing information BTC
  price and plot the price versus date.

## Result:
![](history_price_crypto.png)
**Figure 3**: History BTC/USD price from 2013 to 2020.

## Complete Life Cycle of a Data Science/Machine Learning Project : 
This article describes very nicely the data science life cycle and highlight the main points that a data scientist do in your job


```bash
# requirements for this project.
# Everyone can clone this repo and do the 
# web scraping project.
# requirements.txt
$ pip3 freeze > requirements.txt

# Install pre requirements, make the command below:
$ pip3 install -r requirements.txt
```
### Useful Resources:

 - [Web Scraping with Python and BeautifulSoup](https://medium.com/incedge/web-scraping-bf2d814cc572)
 - [Python Tutorial: Web Scraping with BeautifulSoup and Requests](https://www.youtube.com/watch?v=ng2o98k983k&list=PL-osiE80TeTt2d9bfVyTiXJA-UTHn6WwU&index=46&ab_channel=CoreySchafer)
 - [Web Scraping With Python Using Beautiful Soup](https://medium.com/technofunnel/web-scraping-with-python-using-beautifulsoup-76b710e3e92f)
 - [Project Jupyter website](https://jupyter.org/)
 - [USER DOCUMENTATION: Jupyter Notebook](https://jupyter-notebook.readthedocs.io/en/latest/)
 - [Complete Life Cycle of a Data Science/Machine Learning Project](https://medium.com/swlh/complete-life-cycle-of-a-data-science-machine-learning-project-13df81bbd8eb)
 - [6 Pandas Operations You Should Not Miss](https://medium.com/towards-artificial-intelligence/6-pandas-operations-you-should-not-miss-d531736c6574)
 - [Dash Enterprise](https://plotly.com/)
 - [pandas - examples](https://medium.com/analytics-vidhya/the-simplest-way-to-create-complex-visualizations-in-python-isnt-with-matplotlib-a5802f2dba92)
