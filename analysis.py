# This code shows the main methods 
# available in pandas library

# Author: Andre Vieira da Silva 2020-10-04

# reference: 
# https://medium.com/towards-artificial-intelligence/6-pandas-operations-you-should-not-miss-d531736c6574

import numpy as np
import pandas as pd
import seaborn as sns
df = sns.load_dataset('titanic')

# print head of the dataset
print(df.head())

# the summary stats of each feature
print(df.describe())

# It gives metadata of a dataset
print(df.info())

# Vector indexing is a way to specify the row and column name/integer
smallData = df.loc[[1,7,21,10], ['sex','age','fare','who','class']]
print(smallData)

# matching columns
newData = pd.DataFrame({'sex':['female','male','male'],
                        'age': [25,49,35],
                        'fare':[89.22,70.653,30.666],
                        'who':['child', 'women', 'man'],
                        'class':['First','First','First']})

print(newData)

concat = pd.concat([smallData, newData])

print(concat)