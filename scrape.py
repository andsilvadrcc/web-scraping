# Author: Andre V. Silva 
# importing the libraries
import requests
import urllib
import csv
import json
from functools import reduce
import itertools
import re
import yaml
import numpy
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from urllib.request import urlopen
from bs4 import BeautifulSoup
import numpy as np
import time
import notify2
import pandas as pd
import os.path
import sys
import logging

# https://realpython.com/python-logging/
# Create and configure logger 
logging.basicConfig(filename="logs/file.log", 
                    format='%(asctime)s %(message)s', 
                    filemode='w')

# Creating an object 
logger=logging.getLogger()

############################################################
# functions
# reference
# Building a desktop notification tool for Linux using python
# https://www.codementor.io/@dushyantbgs/building-a-desktop-notification-tool-using-python-bcpya9cwh
def notify():

    ICON_PATH = "628px-Ethereum_logo_2014.svg.png"

    # initialise the d-bus connection
    notify2.init("Cryptocurrency reach the price")

    # create Notification object
    n = notify2.Notification("Crypto Notifier", icon = ICON_PATH)

    # Set the urgency level
    # options:
    # -> notify2.URGENCY_LOW
    # -> notify2.URGENCY_NORMAL
    # -> notify2.URGENCY_HIGH
    n.set_urgency(notify2.URGENCY_NORMAL)

    # Set the timeout
    n.set_timeout(1000)

    result = str(latest_order)

    # Update the content
    n.update("Current price ", result)

    # Show the notification
    n.show()


# Convert list to Dictionary
def listToDict(lst, start, end):
    op = {lst[i]: lst[i + 1] for i in range(start, end, 2)}
    return op

print("********************************************")
print(" Starting the Web scraping and cleaning ! ")
print("********************************************")

if (len(sys.argv) < 3):
    print("Number of arguments is wrong! sorry :(")
    logger.error("Number of arguments is {} and less than 3".format(len(sys.argv)))
    exit()

print("Get info cryptocurrency:.............. ", sys.argv[1])
print("date: Start:.......................... ", sys.argv[2])
print("date: End:............................ ", sys.argv[3])

CRIPTOCURRENCY=sys.argv[1]
date_start=sys.argv[2]
date_end=sys.argv[3]

# Return the last trading
url_thicker ="https://api.bitcointrade.com.br/v3/public/BRL{}/ticker".format(CRIPTOCURRENCY)

# Make a GET request to fetch the raw HTML content
html_cont_thicker = requests.get(url_thicker).text
html_cont_dict_thicker = json.loads(html_cont_thicker)

# get data from html:
parse_data = html_cont_dict_thicker.get('data')

# parse data and get the latest order
latest_order = parse_data.get('last')

# price buy
price_buy = parse_data.get('buy')

# price sell
price_sell = parse_data.get('sell')

# date and time currently
date_time = parse_data.get('date')

#print("date and time {} ".format(date_time))

date = date_time[0:10]
time = date_time[11:19]

# set a range of price
# to nofity the gain.
if( 2316 > latest_order > 2314.90):
    notify()

if( 2600 < latest_order):
    notify()
    
## Select Criptocurrency
## Time: Start and end

# Step 1: Sending a HTTP request to a URL
url = "https://api.bitcointrade.com.br/v3/public/BRL{}/trades?start_time={}T00:00:00-03:00&end_time={}T23:59:59-03:00&page_size=1000&current_page=1".format(CRIPTOCURRENCY, date_start,date_end)

# We use try-except incase the request was unsuccessful because of
# wrong URL or another thing go wrong.
try:
    page = requests.get(url).text
    print("URL has successfully opened!")
except:
    print("Error open the URL")
    logger.error("Error opening the URL")
    sys.exit()
    
# Make a GET request to fetch the raw HTML content
html_cont = requests.get(url).text
html_cont_dict = json.loads(html_cont)

# get data from html:
data_1 = html_cont_dict.get('data')

# info trades
infodata = data_1.get('trades')

# make some ajustment to parsing.
expected_output1=re.sub(":",",",str(infodata))
out1=expected_output1.replace("{", "")
res1=out1.replace("}", "")
data = yaml.full_load(res1)

# size, number, items elements from data,
size=len(data)
number_elem=size/12
number_items = int(number_elem)

jj=0

df = pd.DataFrame()

for j in range(0,number_items):
    dict = listToDict(data,jj,jj+12)
    df = df.append(dict, ignore_index=True)
    jj=jj+12

# cleaning data
del df['active_order_code']

# cleaning data
del df['passive_order_code']

# parsing date
for row in df.itertuples():
    i=row.Index
    date_parse=row.date[0:10]
    df.loc[i,'date']  = date_parse
    #print("date {}, time {}".format(row.date[0:10], row.date[11:25]) ) 
    
# check if the file exist, please.
if os.path.isfile('data/scrapingtrade.csv'):
    print ("File exist, OK")
    FileExist=True
else:
    print ("File does not exist")
    FileExist=False

if(FileExist == False):
    # saving the dataframe
    df.to_csv('data/scrapingtrade.csv')
else:
    # append
    df.to_csv('data/scrapingtrade.csv', mode='a', header=False)

dict_to_df = pd.DataFrame()

df_price_24h = dict_to_df.append(parse_data, ignore_index=True)

# check if the file exist, please.
if os.path.isfile('data/price_24h.csv'):
    print ("File exist, OK")
    FileExist=True
else:
    print ("File does not exist")
    FileExist=False

if(FileExist == False):
    # saving the dataframe
    df_price_24h.to_csv('data/price_24h.csv')
else:
    # append
    df_price_24h.to_csv('data/price_24h.csv', mode='w+', header=True)
