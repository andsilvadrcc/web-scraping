# Analysis data from Bitcointrade:
# Using web scraping to get the data.
# In this analysis:
#   - price buy/sell in the last 24 hours.
#   - book buyers/sellers
 
# Using this data from web scrape, I will
# compare the data info between buyers/sellers

# Author : Andre Vieira da Silva 2020-11-02

# libraries
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.animation as animation

fig = plt.figure()

# Divide the figure into a 1x2 grid, and give me the first section
ax1 = fig.add_subplot(121)

# Divide the figure into a 1x2 grid, and give me the second section
ax2 = fig.add_subplot(122)

def animate(i):
    # read data from csv file
    # trade
    df_trade = pd.read_csv('data/scrapingtrade.csv')

    df_price = pd.DataFrame()

    # count
    number_buyers = 0
    number_sellers = 0
    sum_price_buy = 0
    sum_price_sell = 0

    for row in df_trade.itertuples():
        i=row.Index  
        if(df_trade.loc[i,'type'] == 'buy'):
            price_buy = df_trade.loc[i,'unit_price']
            sum_price_buy = sum_price_buy + price_buy
            number_buyers = number_buyers + 1
        else:
            price_sell = df_trade.loc[i,'unit_price']
            sum_price_sell = sum_price_sell + price_sell
            number_sellers = number_sellers + 1 

    number_total_buysell = number_buyers + number_sellers

    mean_price_buy = round(float(sum_price_buy/number_buyers), 2)
    mean_price_sell = round(float(sum_price_sell/number_sellers), 2)

    #print("Number of buyers is {} and Number of sellers is {} ".format(number_buyers, number_sellers))
    #print("Mean price buyers is {} and mean price of sellers is {} ".format(mean_price_buy, mean_price_sell))

    per_buyers  = round(float(number_buyers/number_total_buysell)*100, 2)
    per_sellers = round(float(number_sellers/number_total_buysell)*100, 2)

    #print("percentage of buyers is {}% and percentage of sellers is {}%".format(per_buyers, per_sellers))

    #print(df_trade)

    # price last 24hrs
    df_price_24h = pd.read_csv('data/price_24h.csv')


    #df_filter = (df_trade['type'] == 'buy')

    #df_mean_price = df_trade[df_filter]

    #print(df_price_24h)


    buy = df_price_24h.iloc[0]['buy']
    sell = df_price_24h.iloc[0]['sell']
    high = df_price_24h.iloc[0]['high']
    low = df_price_24h.iloc[0]['low']
    last = df_price_24h.iloc[0]['last']

    buy = round(buy,2)
    sell = round(sell,2)
    high = round(high,2)
    low = round(low,2)
    last = round(last,2)

    #print("buy = {}, sell = {}, high = {}, low = {} and last = {}".format(buy, sell, high, low, last))

    print("---------------------------------------------")
    print("       |    24h     | <price>   |      %     ")
    print("----------------------------------------------")
    print("  buy  | {}    |  {}  | {} ".format(buy, mean_price_buy, per_buyers))
    print("  sell | {}    |  {}  | {} ".format(sell, mean_price_sell, per_sellers))
    print("  high | {}    |  -------  | -- ".format(high))
    print("  low  | {}    |  -------  | -- ".format(low))
    print("  last | {}    |  -------  | -- ".format(last))
    print("---------------------------------")
    per_highTolast = round(((last - high)/high)*100, 2)
    per_lastTolow =  round(((last - low)/last)*100, 2)
    print(" last - high | {}%".format(round(per_highTolast)))
    print(" last - low  | {}% ".format(per_lastTolow))

    df_pie = pd.DataFrame({'Percentage':[per_buyers, per_sellers]}, index=['Buyers', 'Sellers'])

    my_labels = 'Buyers','Sellers'
    my_colors = ['green','red']
    my_colors_bar = ['red','blue']

    price_highlow = [high, low]
    price_lastlow = [last, last]
    index = ['high-last', 'low-last']
    df = pd.DataFrame({'price_highlow': price_highlow,
                    'price_lastlow': price_lastlow}, index=index)

    ax1.clear()
    ax2.clear()
    
    df_pie.plot.pie(y='Percentage', labels=my_labels,figsize=(15, 12),textprops={'fontsize': 16}, autopct='%1.2f%%', colors=my_colors, ax=ax1)
    df.plot.bar(figsize=(15, 10), rot=0,color=my_colors_bar, ax=ax2)
    ax2.set_title('Price high/low compare to the last price')
    ax2.set_ylim(high-200, high+100)
    plt.savefig('../../Dropbox/buy_sell.pdf')
    
ani = animation.FuncAnimation(fig, animate, interval=1000)    
plt.show()
