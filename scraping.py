##############################################
## This example shows the idea of how to
## extract data from webpages using python.
##############################################

## Author: Andre Vieira da Silva 2020-09-23

#import sys
#sys.path.insert(0, "/usr/lib/python3/dist-packages")

# importing the libraries
import requests
import urllib
import csv
import json
from functools import reduce
import itertools
import re
import yaml
import numpy
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from urllib.request import urlopen
from bs4 import BeautifulSoup
import numpy as np
import time
import notify2
import pandas as pd
import os.path
import sys

############################################################
# scraping.py:
#   - scraping data from bitcointrade and store file.txt to
#   to plot using matplotlib.
############################################################

############################################################
# functions
# reference
# Building a desktop notification tool for Linux using python
# https://www.codementor.io/@dushyantbgs/building-a-desktop-notification-tool-using-python-bcpya9cwh
def notify():

    ICON_PATH = "628px-Ethereum_logo_2014.svg.png"

    # initialise the d-bus connection
    notify2.init("Cryptocurrency reach the price")

    # create Notification object
    n = notify2.Notification("Crypto Notifier", icon = ICON_PATH)

    # Set the urgency level
    # options:
    # -> notify2.URGENCY_LOW
    # -> notify2.URGENCY_NORMAL
    # -> notify2.URGENCY_HIGH
    n.set_urgency(notify2.URGENCY_NORMAL)

    # Set the timeout
    n.set_timeout(1000)

    result = str(latest_order)

    # Update the content
    n.update("Current price ", result)

    # Show the notification
    n.show()


# Convert list to Dictionary
def listToDict(lst, start, end):
    op = {lst[i]: lst[i + 1] for i in range(start, end, 2)}
    return op

###########################################################

print("*****************************")
print(" Starting the Web scraping! ")
print("*****************************")

if (len(sys.argv) < 3):
    print("Number of arguments is less 3. Exit now")
    exit()

print("Get info cryptocurrency:.............. ", sys.argv[1])
print("date: Start:.......................... ", sys.argv[2])
print("date: End:............................ ", sys.argv[3])

CRYPTOCURRENCY=sys.argv[1]
date_start=sys.argv[2]
date_end=sys.argv[3]

# Return the last trading
url_thicker ="https://api.bitcointrade.com.br/v3/public/BRL{}/ticker".format(CRYPTOCURRENCY)

# Make a GET request to fetch the raw HTML content
html_cont_thicker = requests.get(url_thicker).text
html_cont_dict_thicker = json.loads(html_cont_thicker)

# get data from html:
parse_data = html_cont_dict_thicker.get('data')

# parse data and get the latest order
latest_order = parse_data.get('last')

# price buy
price_buy = parse_data.get('buy')

# price sell
price_sell = parse_data.get('sell')

# date and time currently
date_time = parse_data.get('date')

print("date and time {} ".format(date_time))

date = date_time[0:10]
time = date_time[11:19]

# set a range of price
# to nofity the gain.
if CRYPTOCURRENCY == 'BTC':
    if(260000  > latest_order > 245182):  # price in BRL
        notify()

    if( 320000 < latest_order):
        notify()
# ETH, the second cryptocurrency on the market cap
else:
    if(8300 > latest_order > 7900): # price in BRL
        notify()

    if(11500  < latest_order): # price in BRL
        notify()

    if(7500 < latest_order):
	    notify()

## Select CRYPTOCURRENCY
## Time: Start and end

# Step 1: Sending a HTTP request to a URL
url = "https://api.bitcointrade.com.br/v3/public/BRL{}/trades?start_time={}T00:00:00-03:00&end_time={}T23:59:59-03:00&page_size=1000&current_page=1".format(CRYPTOCURRENCY, date_start,date_end)

# We use try-except incase the request was unsuccessful because of
# wrong URL or another thing go wrong.

try:
    page = requests.get(url).text
    print("URL has successfully opened!")
except:
    print("Error opening the URL")
    sys.exit()

# Make a GET request to fetch the raw HTML content
html_cont = requests.get(url).text
html_cont_dict = json.loads(html_cont)

# get data from html:
data_1 = html_cont_dict.get('data')

# info trades
infodata = data_1.get('trades')

# make some ajustment to parsing.
expected_output1=re.sub(":",",",str(infodata))
out1=expected_output1.replace("{", "")
res1=out1.replace("}", "")
data = yaml.full_load(res1)

# size, number, items elements from data,
size=len(data)
number_elem=size/12
number_items = int(number_elem)

# vector with info:
# 1) Operation type: buy or sell
# 2) Amount of coins
# 3) Price at the moment.
vec_type_amount_price = numpy.zeros(shape=(3,number_items))

# variables set to zero.
count_buyers=0
count_sellers=0

tot_amount_buy=0
tot_amount_sell=0

### get info - buy/sell :
###  1) type oder
###  2) amount
###  3) price

jj=0

for j in range(0, number_items):

    # convert list to Dict.
    lst1=listToDict(data,jj,jj+12)
    type_order=lst1.get('type')
    if( type_order == "buy"):
        type_order=0
        count_buyers = count_buyers + 1
    else:
        type_order=1
        count_sellers = count_sellers + 1

    vec_type_amount_price[0][j] =type_order

    amount_order=lst1.get('amount')
    vec_type_amount_price[1][j] =amount_order

    price_unit=lst1.get('unit_price')
    vec_type_amount_price[2][j]= price_unit

    jj = jj+12

# get the total of buyers and sellers
total_buy_sell = count_buyers + count_sellers

# precision
prec =5

# percentage of buyers and sellers
per_buyers=float(count_buyers)
per_sellers=float(count_sellers)

# set precision
per_buyers=round(per_buyers/total_buy_sell, prec)
per_sellers=round(per_sellers/total_buy_sell, prec)

# print the info percentage of buyers/sellers:
print("Number of buyers = ", round(per_buyers, prec))
print("Number of sellers = ", round(per_sellers, prec))

# get the total of amount of the coin buyers/sellers
for j in range(0,number_items):
    if(vec_type_amount_price[0][j] == 0):
        tot_amount_buy = tot_amount_buy + vec_type_amount_price[1][j]
    else:
        tot_amount_sell = tot_amount_sell + vec_type_amount_price[1][j]

tot_amount_buy_sell = tot_amount_buy + tot_amount_sell

per_qtd_buyers=round(tot_amount_buy/tot_amount_buy_sell, prec)
per_qtd_sellers=round(tot_amount_sell/tot_amount_buy_sell, prec)

print("qtd. total buy  = ", per_qtd_buyers)
print("qtd. total sell = ", per_qtd_sellers)

amount_biggest_buy=0
amount_biggest_sell=0

### get the biggest amount buy/sell at the moment!
for j in range(0, number_items):
    if(vec_type_amount_price[0][j] == 0):
        if( vec_type_amount_price[1][j] >= amount_biggest_buy):
            amount_biggest_buy = vec_type_amount_price[1][j]
    else:
        if( vec_type_amount_price[1][j] >= amount_biggest_sell):
            amount_biggest_sell = vec_type_amount_price[1][j]

print("total amount buy  = ", round(tot_amount_buy, prec))
print("total amount sell = ", round(tot_amount_sell, prec))

print("the biggest amount buy  = ", round(amount_biggest_buy, prec))
print("the biggest amount sell = ", round(amount_biggest_sell, prec))

per_biggest_buy = round(amount_biggest_buy/tot_amount_buy, prec)
per_biggest_sell = round(amount_biggest_sell/tot_amount_sell, prec)

print("percentage of the biggest amount buy of the total  = ", per_biggest_buy)
print("percentage of the biggest amount sell of the total = ", per_biggest_sell)

# percentage...
per_buyers = round(per_buyers*100, prec)
per_sellers = round(per_sellers*100, prec)

per_qtd_buyers = round(per_qtd_buyers*100, prec)
per_qtd_sellers = round(per_qtd_sellers*100, prec)

per_biggest_buy = round(per_biggest_buy*100, prec)
per_biggest_sell = round(per_biggest_sell*100, prec)

### save data to file.txt
f = open('data/infodata.txt', 'w+')
# write info data in the file.
f.write("%0.2f %5.2f\n" % (per_buyers, per_sellers))
f.write("%0.2f %5.2f\n" % (per_qtd_buyers, per_qtd_sellers))
f.write("%0.2f %5.2f\n" % (per_biggest_buy, per_biggest_sell))

# check if the file exist, please.
if os.path.isfile('data/infoBuyersSellers.csv'):
    print ("File exist")
    FileExist=True
else:
    print ("File not exist")
    FileExist=False

if(FileExist == False):
    dict_df = {'per_buyers'     : [per_buyers],
           'per_qtd_buyers'     : [per_qtd_buyers],
           'per_biggest_buy'    : [per_biggest_buy],
           'per_sellers'        : [per_sellers],
           'per_qtd_sellers'    : [per_qtd_sellers],
           'per_biggest_sell'   : [per_biggest_sell],
           'price_buy'          : [price_buy],
           'price_sell'         : [price_sell],
           'time'               : [time],
           'date'               : [date],
           }
    # DataFrame
    df = pd.DataFrame(dict_df)

    # saving the dataframe
    df.to_csv('data/infoBuyersSellers.csv')
else:
    dict_df = {'per_buyers' : [per_buyers],
           'per_qtd_buyers' : [per_qtd_buyers],
           'per_biggest_buy' : [per_biggest_buy],
           'per_sellers' : [per_sellers],
           'per_qtd_sellers' : [per_qtd_sellers],
           'per_biggest_sell' : [per_biggest_sell],
           'price_buy'  : [price_buy],
           'price_sell' : [price_sell],
           'time'       : [time],
           'date'       : [date],
           }
    # DataFrame
    df = pd.DataFrame(dict_df)
    # append
    df.to_csv('data/infoBuyersSellers.csv', mode='a', header=False)

#time.sleep(30)
print("data save! all done, :)")
