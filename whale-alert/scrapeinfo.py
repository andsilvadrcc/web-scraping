# This script is using Whale Alert API
# to get real-time information about the 
# transactions.

# https://whale-alert.io/ (see this for more info)

#https://pypi.org/project/whale-alert/
#https://github.com/stuianna/whaleAlert

import time
import pandas as pd
from datetime import datetime
from pprint import pprint  # For formatted dictionary printing
from whalealert.whalealert import WhaleAlert
whale = WhaleAlert()

# To print colored text in python 
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


# get key credencial for API
API_KEY = open('key_credencial.txt', mode='r').read() 

for i in range(0,100000):

    # Specify a single transaction from the last 10 minutes
    start_time_ts = int(time.time()+2000)

    now = datetime.fromtimestamp(start_time_ts)
    transaction_count_limit = 100 

    #print(f'timestamp..............: {start_time_ts}')
    #print(f'datetime...............: {now}')

    success, transactions, status = whale.get_transactions(start_time_ts, api_key=API_KEY, limit=transaction_count_limit)

    if(success == 'False'):
        exit('success: False, exit now')

    dict={}
    for k,v in [(key,d[key]) for d in transactions for key in d]:
        if k not in dict: dict[k]=[v]
        else: dict[k].append(v)

    df = pd.DataFrame(dict)

    #df.to_csv('../data/whale-alert.csv')

    # drop columns that are not important in the moment:
    #df = df.drop(columns=['blockchain', 'id', 'transaction_type', 'hash', 'transaction_count'])
    df = df.drop(columns=['blockchain', 'hash', 'transaction_count'])

    # Select only BTC or USDT
    df = df[(df.symbol == 'BTC') | (df.symbol == 'USDT')]

    for row in df.index:
        currency = df['symbol'][row]
        amount = df['amount'][row] 
        timestamp = datetime.fromtimestamp(df['timestamp'][row])
        amount_usd = df['amount_usd'][row]
        dict_from_wallet = df['from'][row] # dict
        dict_to_wallet = df['to'][row] # dict

        id_transaction = df['id'][row]
        transaction_type = df['transaction_type'][row] 

        # get info about the wallet 'from' --> 'to'
        # owner and owner type
        owner_from_wallet = dict_from_wallet['owner']  
        owner_type_from_wallet = dict_from_wallet['owner_type']

        owner_to_wallet = dict_to_wallet['owner']  
        owner_type_to_wallet = dict_to_wallet['owner_type']

        if(owner_from_wallet == ''):
            owner_from_wallet = 'unknown'
        if(owner_type_from_wallet == ''):
            owner_type_from_wallet = 'unknown'

        if(owner_to_wallet == ''):
            owner_to_wallet = 'unknown'
        if(owner_type_to_wallet == ''):
            owner_type_to_wallet = 'unknown'   


        if( amount >= 2000 and currency=='BTC'):
            print('***********************************************************************************')
            print('>>>>>>>>>> WARNING: WHALE MOVING FUNDS <<<<<<<<<<<<<')
            print(f'ATTENTION MORE THAN {2000} BTC MOVED')
                
            price_now = round(amount_usd/amount, 2)
            print(f'# {row} {bcolors.FAIL}{timestamp}{bcolors.ENDC} price 1 BTC ({price_now})')
            print(f'{bcolors.OKGREEN}{amount}{bcolors.ENDC} {bcolors.OKBLUE}{currency}{bcolors.ENDC} ({amount_usd} USD)')
            print(f'#id transaction: {id_transaction} transferred from {bcolors.OKCYAN}{owner_from_wallet}{bcolors.ENDC} to {bcolors.OKCYAN}{owner_to_wallet}{bcolors.ENDC} wallet')
            print('***********************************************************************************')
            time.sleep(3)

        if((2000 > amount >= 1500) and currency=='BTC'):
            print('***********************************************************************************')
            print('>>>>>>>>>> WARNING: WHALE MOVING FUNDS <<<<<<<<<<<<<')
            print(f'ATTENTION MORE THAN {1500} BTC MOVED')
                
            price_now = round(amount_usd/amount, 2)
            print(f'# {row} {bcolors.FAIL}{timestamp}{bcolors.ENDC} price 1 BTC ({price_now})')
            print(f'{bcolors.OKGREEN}{amount}{bcolors.ENDC} {bcolors.OKBLUE}{currency}{bcolors.ENDC} ({amount_usd} USD)')
            print(f'#id transaction: {id_transaction} transferred from {bcolors.OKCYAN}{owner_from_wallet}{bcolors.ENDC} to {bcolors.OKCYAN}{owner_to_wallet}{bcolors.ENDC} wallet')
            print('***********************************************************************************')
            time.sleep(3)

        if((1500 > amount >= 1000) and currency=='BTC'):
            print('***********************************************************************************')
            print('>>>>>>>>>> WARNING: WHALE MOVING FUNDS <<<<<<<<<<<<<')
            print(f'ATTENTION MORE THAN {1000} BTC MOVED')
                
            price_now = round(amount_usd/amount, 2)
            print(f'# {row} {bcolors.FAIL}{timestamp}{bcolors.ENDC} price 1 BTC ({price_now})')
            print(f'{bcolors.OKGREEN}{amount}{bcolors.ENDC} {bcolors.OKBLUE}{currency}{bcolors.ENDC} ({amount_usd} USD)')
            print(f'#id transaction: {id_transaction} transferred from {bcolors.OKCYAN}{owner_from_wallet}{bcolors.ENDC} to {bcolors.OKCYAN}{owner_to_wallet}{bcolors.ENDC} wallet')
            print('***********************************************************************************')
            time.sleep(3)
    
        if((1000 > amount >= 500) and currency=='BTC'):
            print('***********************************************************************************')
            print('>>>>>>>>>> WARNING: WHALE MOVING FUNDS <<<<<<<<<<<<<')
            print(f'ATTENTION MORE THAN {500} BTC MOVED')
                
            price_now = round(amount_usd/amount, 2)
            print(f'# {row} {bcolors.FAIL}{timestamp}{bcolors.ENDC} price 1 BTC ({price_now})')
            print(f'{bcolors.OKGREEN}{amount}{bcolors.ENDC} {bcolors.OKBLUE}{currency}{bcolors.ENDC} ({amount_usd} USD)')
            print(f'#id transaction: {id_transaction} transferred from {bcolors.OKCYAN}{owner_from_wallet}{bcolors.ENDC} to {bcolors.OKCYAN}{owner_to_wallet}{bcolors.ENDC} wallet')
            print('***********************************************************************************')
            time.sleep(3)

        if( amount < 500 and currency=='BTC'):
            print(f'# {row} {bcolors.FAIL}{timestamp}{bcolors.ENDC} {bcolors.OKGREEN}{amount}{bcolors.ENDC} {bcolors.OKBLUE}{currency}{bcolors.ENDC} ({amount_usd} USD) transferred from {bcolors.OKCYAN}{owner_from_wallet}{bcolors.ENDC} to {bcolors.OKCYAN}{owner_to_wallet}{bcolors.ENDC} wallet')
            time.sleep(3)