# This code just do the download
# of one file json and use pandas
# to plot the data.
# Author: Andre Vieira da Silva date 2020-09-30

# - This is the API for CRYPTOCURRENCY/USD price request.
import json
import urllib.request
import pandas as pd
import csv
import matplotlib.pyplot as plt
from datetime import date
import requests
import notify2
from IPython.display import clear_output
import time
from matplotlib.animation import FuncAnimation
from datetime import datetime
import os.path
import sys
from icecream import ic

# To print colored text in python 
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

# setting figures multi plots
fig = plt.figure(figsize=(12,8)) # 12 is width, 12 is height
ax1 = plt.subplot(1,2,1) # nrows = 1, ncols = 2 and numbers of subplots = 1
ax2 = plt.subplot(1,2,2) # nrows = 1, ncols = 2 and numbers of subplots = 2

plt.cla()

# Building a desktop notification tool for Linux using python
# source: https://www.codementor.io/@dushyantbgs/building-a-desktop-notification-tool-using-python-bcpya9cwh
def notify(price_crypto_usd):

    ICON_PATH = "icons8-bitcoin-64.png"

    # initialise the d-bus connection
    notify2.init(f"{bcolors.OKGREEN}Cryptocurrency reach the target price{bcolors.ENDC}")

    # create Notification object
    n = notify2.Notification("Crypto Notifier", icon = ICON_PATH)

    # Set the urgency level
    # options:
    # -> notify2.URGENCY_LOW
    # -> notify2.URGENCY_NORMAL
    # -> notify2.URGENCY_HIGH
    n.set_urgency(notify2.URGENCY_NORMAL)

    # Set the timeout
    n.set_timeout(1000)

    result = str(price_crypto_usd)

    # Update the content
    n.update("Current price ", result)

    # Show the notification
    n.show()

CRYPTOCURRENCY=sys.argv[1]

if (len(sys.argv) < 1):
    print(f"{bcolors.FAIL}Error: number of arguments is less 3. Exit now{bcolors.ENDC}")
    exit()

print("Get info cryptocurrency:.............. ", CRYPTOCURRENCY)

count=0

def animate(i):    
    global count
    from datetime import date 
    
    date_today=date.today()
    now=datetime.now() 
    current_time = now.strftime("%H:%M:%S")

    # cryptocurrency
    # request
    data_crypto = requests.get(f'https://production.api.coindesk.com/v1/currency/ticker?currencies={CRYPTOCURRENCY}').json()
    price_crypto = data_crypto['data']['currency'][CRYPTOCURRENCY]['quotes']['USD']['price']

    # get json from coindesk
    # API: https://www.coindesk.com/coindesk-api
    # full history of the CRYPTOCURRENCY price:
    # start 2013-10-01
    start_date="2016-10-01"
    
    url = f'https://api.coindesk.com/v1/bpi/historical/close.json?start={start_date}&end={date_today}'
    hdr = { 'User-Agent' : 'Mozilla/5.0 (Windows NT 6.1; Win64; x64)' }

    # request
    req = urllib.request.Request(url, headers=hdr)
    readdata = urllib.request.urlopen(req)

    # read json
    json_data = readdata.read()

    # decode type 'bytes' to string
    data_string = json_data.decode('utf-8')

    # load dictionary
    data_dict = json.loads(data_string)

    #parse data
    info_data = data_dict.get('bpi')

    # file csv
    f = open('data/data_'+CRYPTOCURRENCY+'.csv', 'w+')
    
    # header file.
    f.write("date,price\n")

    # loop over dict.
    for key, value in info_data.items():
        # write info date / price cryptocurrency into csv.
        f.write("{}, {}\n".format(key, value))

    # close file
    f.close()

    # plotting...
    # file csv to dataframe
    df = pd.read_csv('data/data_'+CRYPTOCURRENCY+'.csv')

    price_higher = 57441.6

    for i in range(len(df)): # loop dataframe
        price=df.loc[i, "price"]
        date=df.loc[i, "date"]

        # looking for the price higher
        if price >= price_higher:
                price_higher = price
                date_higher = date

    # type casting
    price_crypto_usd=round(float(price_crypto), 2)
    
    # create file
    fday = open('data/data_day_'+CRYPTOCURRENCY+'.csv', 'a+')

    # write
    if(count==0):
        fday.write("time,price\n") # intraday
    
    # write file intraday
    fday.write("{}, {}\n".format(current_time, price_crypto_usd))
    
    # close file
    fday.close()
    
    if(price_crypto_usd > price_higher):  # price USD
        notify(price_crypto_usd)

        print(f"# {count} - Price higher all the time for 1 {CRYPTOCURRENCY}: {price_higher} USD date {date_higher}")
        print("****************************************************************************************")
        print(f"# {count} - The current market price reaches maximum price (day):{bcolors.FAIL} {price_crypto_usd}{bcolors.ENDC} time: {current_time}")
        print("****************************************************************************************")
    else:
        print(f"# {count} - 1 {CRYPTOCURRENCY}:{bcolors.HEADER} {price_crypto_usd}{bcolors.ENDC} {bcolors.OKCYAN}(ATH {price_higher}){bcolors.ENDC} USD today's date {date_today} time: {current_time}")
        
    # plot using the dataframe
    df.plot(kind='line', ax=ax1,x='date',y='price', legend=False,
            color='black', grid='on', title=f'{CRYPTOCURRENCY}/USD - full history until now')
    
    # line straight higher price
    ax1.axhline(y=price_higher, color='r', linestyle='--')
    ax1.set_ylabel(price)

    # This is important to not reach the limit of request HTTP, in API coindesk
    # In the future, change the request to coin market cap.
    time.sleep(3) 
    
    ### intraday
    df_day = pd.read_csv('data/data_day_'+CRYPTOCURRENCY+'.csv')
    df_day.plot(kind='line', ax=ax2,x='time',y='price', color='blue', legend=False, grid='on', title=f'{CRYPTOCURRENCY}/USD - day')
    
    price_higher_day = 0
    price_lower_day = df_day.loc[0, "price"]
    
    # loop dataframe
    for i in range(len(df_day)):
        price=df_day.loc[i, "price"]
            
        # looking for the price higher and price lower
        if price >= price_higher_day:
            price_higher_day = price
        if price < price_lower_day:
            price_lower_day = price


    print(f"price day - lower:{bcolors.FAIL} {price_lower_day}{bcolors.ENDC} and higher:{bcolors.OKGREEN} {price_higher_day}{bcolors.ENDC}")
    
    # line straight higher price
    ax2.axhline(y=price_higher_day, color='g', linestyle='--')
    
    # line straight lower price
    ax2.axhline(y=price_lower_day, color='r', linestyle='--')
    
    count += 1
    
ani = FuncAnimation(plt.gcf(), animate, interval=1000)

plt.savefig('history_price_crypto.png')
plt.show()