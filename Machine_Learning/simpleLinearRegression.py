# Import the necessary libraries
import numpy
import matplotlib.pyplot as plot
import pandas
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression

# To use LaTeX font as your default 
# font in Matplotlib
plot.rcParams['text.usetex'] = True

# Import the dataset
dataset = pandas.read_csv('../data/infoBuyersSellers.csv')
x = dataset.iloc[:, 1:2].values
y = dataset.iloc[:, 7:8].values


# Split the dataset into the training set and test set
# We're splitting the data in 1/3 for the training set
# and 2/3 will go to testing set.
xTrain, xTest, yTrain, yTest = train_test_split(x, y, test_size = 1/3, random_state = 0)

## Creating a LinearRegression object and fitting it
## on our training set.
linearRegressor = LinearRegression()
linearRegressor.fit(xTrain, yTrain)
#
## Predicting the test set results
yPrediction = linearRegressor.predict(xTest)

# Visualising the training set results
plot.scatter(xTrain, yTrain, color = 'black')
# to change the figure size
plot.figure(figsize=(7, 4.5))
plot.plot(xTrain, linearRegressor.predict(xTrain), color = 'blue')
plot.title('$y = f(x)$')
plot.xlabel('$x$')
plot.ylabel('$y$')
plot.show()

# Visualising the test set results
plot.scatter(xTest, yTest, color = 'black')
plot.plot(xTrain, linearRegressor.predict(xTrain), color = 'red')
plot.title('y = f(x)')
plot.xlabel('$x$')
plot.ylabel('$y$')
plot.show()