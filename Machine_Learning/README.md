# Machine Learning

Here I will go to do the documentation of the my studies in Machine Learning. This is just for a personal reference.

- Supervised Learning
  - Classification
  - Regression
- Unsupervised Learning

## Big picture

![](big_picture_ML.png)

**Figure 1**: [[Machine Learning Algorithms](https://medium.com/swlh/63-machine-learning-algorithms-introduction-5e8ea4129644)].

### Basic concepts 

- Define objective
- Collect data
- Prepare data
- Select algorithm
- Train model
- Test model
- Predict
- Deploy

## CS50’s Introduction to Artificial Intelligence with Python

This course explores the concepts and algorithms at the foundation of modern artificial intelligence, diving into the ideas that give rise to technologies like game-playing engines, handwriting recognition, and machine translation. For more information see this page [CS50-AI](https://cs50.harvard.edu/ai/2020/).

  - [Lecture 0 - Search](search):
    - Uninformed search: Search strategy that uses no problem-specific knowledge
      - Depth First Search ([DFS](search/mazeDFS.py))
        ```bash
        $ python mazeDFS.py maze2.txt
        Solving...
        States Explored: 194
        Solution:
        ```
        Below the result from DFS.
        ![](search/mazeDFS.png) **Depth First Search**
      - Breadth First Search (BFS)
        ```bash
        $ python mazeBFS.py maze2.txt
        Solving...
        States Explored: 77
        Solution:
        ```
        Below the result from BFS.
        ![](search/mazeBFS.png) **Breadth First Search**
  - Lecture 1 - Knowledge
  - Lecture 2 - Uncertainty
  - Lecture 3 - Optimization
  - Lecture 4 - Learning
  - Lecture 5 - Neural Networks
  - Lecture 6 - Language

### Books and useful resources:
- [Introduction to Machine Learning with Python](https://www.oreilly.com/library/view/introduction-to-machine/9781449369880/) 
  - [GitHub](https://github.com/amueller/introduction_to_ml_with_python)
- [63 Machine Learning Algorithms — Introduction](https://medium.com/swlh/63-machine-learning-algorithms-introduction-5e8ea4129644)
- [https://cs50.harvard.edu/ai/2020/](https://cs50.harvard.edu/ai/2020/)