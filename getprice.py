import requests

# >>> criptocurrency BTC
data_BTC = requests.get('https://production.api.coindesk.com/v1/currency/ticker?currencies=BTC').json()
price_btc = data_BTC['data']['currency']['BTC']['quotes']['USD']['price']

print("The currently price for 1 BTC is about %f USD" % float(price_btc))


# >>> criptocurrency ETH
data_ETH = requests.get('https://production.api.coindesk.com/v1/currency/ticker?currencies=ETH').json()
price_eth = data_ETH['data']['currency']['ETH']['quotes']['USD']['price']

print("The currently price for 1 ETH is about %f USD" % float(price_eth))
