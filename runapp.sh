#!/bin/zsh

echo "Running app..."
python plotdata.py BTC &
 
condition=$(< filecond.txt)

while $condition
do    

    python scraping.py BTC 2020-10-05 2021-12-02
    #python scraping.py ETH 2020-10-05 2021-12-02
    sleep 5s
    condition=$(< filecond.txt)
done
