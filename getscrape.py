import requests 
from bs4 import BeautifulSoup 

URL = "http://learn-javascript.in/"
r = requests.get(URL) 

soup = BeautifulSoup(r.content, 'html5lib') 

table = soup.findAll('h1', attrs = {'class':'content-heading'}) 

for row in table:
    print(row.text)